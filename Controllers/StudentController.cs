using System;
using Microsoft.AspNetCore.Mvc;
using InterfaceAndServices.Services;
using InterfaceAndServices.Models;

namespace InterfaceAndServices.Controllers
{
    [ApiController]
    [Route("api/student")]
    public class StudentController : Controller
    {
        public readonly IStudent studentService;
 	      public StudentController(IStudent IStudentService)
 	      {
 	          studentService = IStudentService;
 	      }
            [HttpGet ("list")]
            public IActionResult Get () {
                return Ok(studentService.Get());
            }
           
            [HttpGet ("single/{id}")]
        public ActionResult GetSingleById (int id) {
            return Ok(studentService.GetSingleById(id));
        }
        [HttpPost ("create")]
        public ActionResult Post (StudentModel student) {
            return Ok(studentService.Post(student));
        }
        [HttpPut ("update/{id}")]
        public ActionResult Put (StudentModel student, int id) {
             return Ok(studentService.Put(student,id));
        }
         [HttpDelete ("delete/{id}")]
        public ActionResult Delete (int id) {
            return Ok(studentService.Delete(id));
        }

    }
}