using System;
namespace InterfaceAndServices.Models
{
    public class StudentModel
    {
        public int Id{get;set;}
        public string Name{get;set;}
        public int Gender{get;set;}
        public int Department{get;set;}
        public DateTime DateOfBirth{get;set;}
        public int MarksPercentage{get;set;}
    }
}