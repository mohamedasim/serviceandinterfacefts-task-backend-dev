using InterfaceAndServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace InterfaceAndServices.Services
{
    public class IStudent : Controller
    {
         IConfiguration Configure;
        
        public IStudent (IConfiguration configuration) {
            Configure = configuration;
        }

        
        public IActionResult Get () {
            string query = @"select * from Students";
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                return Ok(table);
        }

        
        public ActionResult GetSingleById (int id) {
            string query = @"select * from Students where Id ='" + id + "'";
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                return new JsonResult (table);
        }

      
        public ActionResult Post (StudentModel student) {
            string query = @"insert into  Students values('" + student.Name + @"','" + student.Department + @"','" + student.Gender + @"','" + student.DateOfBirth + @"','" + student.MarksPercentage + @"')";
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                return new JsonResult ("your Data Created Successfully!");
        }

        public ActionResult Put (StudentModel student, int id) {
            
                string query = @"update Students set Name ='" + student.Name + @"',Department='" + student.Department + @"',Gender='" + student.Gender + @"',DateOfBirth='" + student.DateOfBirth + @"',MarksPercentage='" + student.MarksPercentage + @"'where Id='" + id + "'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                return new JsonResult ("your Data Updated Successfully!");
            
        }

        public ActionResult Delete (int id) {
            
                string query = @"delete from Students where Id='" + id + @"'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                
                return new JsonResult ("your Requested Data has been deleted Successfully!");
            
        }
    }
}